<?php
/**
 * Created by PhpStorm.
 * User: robert
 * Date: 18.12.2018
 * Time: 00:34
 */

namespace TreeVert\NextGenImages\Converter;


final class RequestHandler
{
    private $originalFile;
    private $formatProcessors = [];

    /**
     * RequestHandler constructor.
     *
     * @param $originalFile
     */
    private final function __construct($originalFile)
    {
        $this->originalFile = $originalFile;

        $this->addFormatProcessor(new WebpFormatProcessor())
            ->addFormatProcessor(new Jp2FormatProcessor())
            ->addFormatProcessor(new JxrFormatProcessor());
    }

    public final static function forFile($file)
    {
        return new RequestHandler($file);
    }

    public final function outputSupportedFormat($destination = "php://stdout")
    {
        $imageFile = $this->getFirstSupportedFormatProcessor()->convert($this->originalFile);

        $contentType = mime_content_type($imageFile);

        header("Content-Type: $contentType");
        $this->readFile($destination, $imageFile);

        return $this;
    }

    private final function readFile($destination, $imageFile)
    {
        if ($destination === "php://stdout") {
            readfile($imageFile, false);
        } else {
            $resource = fopen($destination, "r");
            readfile($imageFile, false, $resource);
            fclose($resource);
        }
    }

    /**
     * @return FormatConverter
     */
    private final function getFirstSupportedFormatProcessor()
    {
        foreach ($this->formatProcessors as $processor) {
            if ($processor->isSupported()) {
                return $processor;
            }
        }

        return PassThroughFormatProcessor::getInstance();
    }

    private final function addFormatProcessor(FormatProcessor $formatDetector)
    {
        $this->formatProcessors[] = $formatDetector;

        return $this;
    }
}