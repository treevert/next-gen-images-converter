<?php
/**
 * Created by PhpStorm.
 * User: robert
 * Date: 18.12.2018
 * Time: 00:49
 */

namespace TreeVert\NextGenImages\Converter;


class Jp2FormatProcessor implements FormatProcessor
{

    public function isSupported()
    {
        $acceptsJp2 = strstr(Headers::getAccept(), "image/jp2") !== false;
        $isSafari = strstr(Headers::getUserAgent(), 'Safari') !== false;

        return $acceptsJp2 || $isSafari;
    }

    public function convert($path)
    {
        // TODO: Implement convert() method.
        return $path;
    }
}