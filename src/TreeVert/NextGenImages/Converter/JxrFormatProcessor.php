<?php
/**
 * Created by PhpStorm.
 * User: robert
 * Date: 18.12.2018
 * Time: 00:49
 */

namespace TreeVert\NextGenImages\Converter;


class JxrFormatProcessor implements FormatProcessor
{

    public function isSupported()
    {
        $acceptsJxr = strpos(Headers::getAccept(), "image/jxr") !== false;
        return $acceptsJxr || $this->getEdgeVersion() >= 11;
    }

    public function convert($path)
    {
        // TODO: Implement convert() method.
        return $path;
    }

    private function getEdgeVersion()
    {
        $edgeVersion = 0;

        $edgeMatches = [];
        if (1 === preg_match("/Edge\/(\d+)/", Headers::getUserAgent(), $edgeMatches) && isset($edgeMatches[1])) {
            $edgeVersion = intval($edgeMatches[1]);
        }

        return $edgeVersion;
    }
}