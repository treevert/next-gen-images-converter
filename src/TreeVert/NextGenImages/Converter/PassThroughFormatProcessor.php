<?php
/**
 * Created by PhpStorm.
 * User: robert
 * Date: 18.12.2018
 * Time: 00:56
 */

namespace TreeVert\NextGenImages\Converter;


final class PassThroughFormatProcessor implements FormatProcessor
{
    private static $instance;

    /**
     * PassThroughFormatProcessor constructor.
     */
    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new PassThroughFormatProcessor();
        }
        return self::$instance;
    }

    public function isSupported()
    {
        return true;
    }

    public function convert($path)
    {
        return $path;
    }
}