<?php
/**
 * Created by PhpStorm.
 * User: robert
 * Date: 18.12.2018
 * Time: 00:56
 */

namespace TreeVert\NextGenImages\Converter;


interface FormatProcessor
{
    public function isSupported();

    public function convert($path);
}