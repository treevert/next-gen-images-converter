<?php
/**
 * Created by PhpStorm.
 * User: robert
 * Date: 18.12.2018
 * Time: 00:36
 */

namespace TreeVert\NextGenImages\Converter;


interface FormatConverter {
	public function convert($path);
}