<?php
/**
 * Created by PhpStorm.
 * User: robert
 * Date: 18.12.2018
 * Time: 00:49
 */

namespace TreeVert\NextGenImages\Converter;


class WebpFormatProcessor implements FormatProcessor
{
    private $canConvert = false;

    /**
     * WebpFormatProcessor constructor.
     */
    public function __construct()
    {
        $this->canConvert = !empty(shell_exec("where cwebp")) || !empty(shell_exec("command -v cwebp"));
    }


    public function isSupported()
    {
        $acceptsWebp = strpos(Headers::getAccept(), "image/webp") !== false;
        return $acceptsWebp;
    }

    public function convert($path)
    {
        $webpPath = $path . ".webp";

        if (!file_exists($webpPath) && $this->canConvert) {
            shell_exec("cwebp -q 87 $path -o $webpPath");
        }

        if (!file_exists($webpPath)) {
            return $path;
        }

        return $webpPath;
    }
}