<?php
/**
 * Created by PhpStorm.
 * User: robert
 * Date: 18.12.2018
 * Time: 00:37
 */

use TreeVert\NextGenImages\Converter\RequestHandler;

$file = __DIR__ . "/" . $_GET["file"];

RequestHandler::forFile($file)->outputSupportedFormat();